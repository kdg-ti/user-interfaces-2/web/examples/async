/*
* We schrijven een functie die een bericht toons met een afzender
**/
{
    function showMessage(from: string, text?: string) { // parameters: from, text
        alert(from + ': ' + text);
    }

    showMessage('Ann', 'Hello!'); // Ann: Hello! (*)
    showMessage('Ann', "What's up?"); // Ann: What's up? (**)
    showMessage("Ann"); // Ann: no text given
}

/*
* Als een functie wordt aangeroepen, maar er wordt geen argument gegeven, dan wordt de bijbehorende waarde undefined.
* De eerder genoemde
*       function showMessage(from, text)
* kan bijvoorbeeld worden aangeroepen met één argument en een default value voor de tweede parameter:
* */
{
    function showMessage2(from: string, text = "no text given") {
        alert(from + ": " + text);
    }

    showMessage2('Ann', 'Hello!'); // Ann: Hello! (*)
    showMessage2('Ann', "What's up?"); // Ann: What's up? (**)
    showMessage2("Ann"); // Ann: no text given
}

/*
* Alternatieven voor default parameters,
* Soms is het zinvol om in een later stadium pas default values voor parameters toe te kennen.
* */
{
    function showMessage3(from: string, text?: string) {

        //... bv als hier nog code komt te staan

        if (text === undefined) { // Als er geen text is meegegeven, dan wordt de default value gebruikt
            text = 'leeg bericht';
        }

        alert(from + ": " + text);
    }

    showMessage3("Ann"); // Ann: empty message
}

/*
Andere modernere/kortere manieren
* */
{
    function showMessageOr(text?: string) {
        // als text undefined is of foutief is, zet het op 'empty'.
        text = text || 'empty';

        console.log(text);
    }

    function showCountNullish(count?: string | number | null) {
        // als count niet undefined of null is, toon dan "unknown".
        console.log(count ?? "unknown");
    }

    showMessageOr(); // empty
    showCountNullish(0); // 0
    showCountNullish(null); // unknown
    showCountNullish(); // unknown
}
