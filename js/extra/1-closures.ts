/* De functie sayHi gebruikt een externe variabele naam.
* Wanneer de functie wordt uitgevoerd, welke waarde gaat hij dan gebruiken?
* */
{
    let name = "John";

    function sayHi() {
        alert("Hi, " + name);
    }

    name = "Pete";

    sayHi(); // Wat wordt het resultaat "John" or "Pete"?
}


/* De functie makeWorker hieronder maakt een andere functie en geeft die terug.
* Die nieuwe functie kan ergens anders vandaan worden aangeroepen.
*
* Heeft hij toegang tot de buitenste variabelen vanaf zijn aanmaakplaats,
* of de aanroepplaats, of beide?
* */
{
    function makeWorker() {
        let name = "Pete";

        return function () {
            alert(name);
        };
    }

    let name = "John";

// maak een funtie
    let work = makeWorker();

// roep ze aan
    work(); // wat zal het resultaat zijn?
}


/*
* Hier maken we twee tellers: counter en counter2 met dezelfde makeCounter-functie.
*
* Zijn ze onafhankelijk? Wat gaat de tweede teller weergeven? 0,1 of 2,3 of iets anders?
* */
{
    function makeCounter() {
        let count = 0;

        return function () {
            return count++;
        };
    }

    let counter = makeCounter();
    let counter2 = makeCounter();

    alert(counter()); // 0
    alert(counter()); // 1

    alert(counter2()); // ?
    alert(counter2()); // ?
}


/*
 * Kijk naar de code.
 * Wat zal het resultaat zijn van de oproep op de laatste regel?
 */
{
    let phrase = "Hello";

    if (true) {
        let user = "John";

        function sayHiAgain() {
            console.log(`${phrase}, ${user}`);
        }
    }

    sayHiAgain();
}


{
    let phrase = "Hello";
    let user: string;
    if (false) {
        console.log('if statement is true');
        user = "John";

        function sayHiAnotherTime() {
            console.log(`${phrase}, ${user}`);
        }
    } else {
        user = "Pete";
    }

    sayHiAnotherTime();
}
