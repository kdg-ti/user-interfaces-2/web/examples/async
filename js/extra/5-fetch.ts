/*
* Haal pokemon #6 op met fetch en Promises
* */
{
    fetch("https://pokeapi.co/api/v2/pokemon/2000", {
        method: "GET",
        headers: {
            "Accept": "application/json"
        }
    })
        .then(resp => {
            if (resp.ok) return resp.json()
            else throw new Error(resp.statusText)
        })
        .then(pokemonObject => {
            typeof alert != "undefined" ? alert(pokemonObject?.name) : console.log(pokemonObject?.name);
            console.log(pokemonObject?.sprites?.front_default);
        })
        .catch(err => console.log(err))
}
