/*
Volgende functie omzetten naar arrow Functies
* */
{
    let sum = function (a: number, b: number) {
        return a + b;
    };
}
/*
* Veranderen we naar
* */
{
    let sum = (a: number, b: number) => a + b;

    let sumWithBraces = (a: number, b: number) => {
        return a + b;
    }

    alert(sum(1, 2)); // 3
}


/*
* Vervang volgende Functie-uitdrukkingen door arrowFuncties in de onderstaande code:
* */
{
    function ask(question: string, yes: () => void, no: () => void) {
        if (confirm(question)) yes();
        else no();
    }

    ask(
        "Do you agree?",
        function () {
            alert("You agreed.");
        },
        function () {
            alert("You canceled the execution.");
        }
    );
}
{
    ask2(
        "Do you agree?",
        () => alert("You agreed."),
        () =>
            alert("You canceled the execution.")
    )

    function ask2(question: string, yes: () => void, no: () => void) {
        if (confirm(question)) yes();
        else no();
    }
}
{
    let ask3 = (question: string, yes: () => any, no: () => any) => confirm(question) ? yes() : no();

    ask3(
        'Do you agree?',
        () => console.log('You agreed'),
        () => console.log('You interrupted execution'),
    );
}

/*
* Objecten kunenn ook methodes bevatten als property.
* */
{
    let user = {
        name: "John",
        sayHi: function () {
            alert("Hello");
        }
    };

// method shorthand ziet er beter uit.
    user = {
        name: "John",
        sayHi() { // gelijk aan "sayHi: function(){...}"
            alert("Hello");
        }
    };
}
/*
* De 'this' parameter
* */
{
    let user = {
        name: "John",
        age: 30,

        sayHi() {
            // "this" is the "current object"
            alert(this.name);
        }

    };

    user.sayHi(); // John
}

/*
* 'this' is de context van een functie declaratie.
* */
{
    let user = {
        firstName: "Ilya",
        sayHi() {
            function innerFunctie() {
                // @ts-ignore -> omdat we anders een error krijgen nog voor we uitvoeren.
                alert(this.firstName); // this.firstName is undefined
            }

            innerFunctie();
        }
    };

    user.sayHi(); // undefined
}
/*
* Arrow functies hebben geen 'this' parameter, en gebruiken de 'this' parameter van de omringende functie.
* */
{
    let user = {
        firstName: "Ilya",
        sayHi() {
            let arrow = () => {
                alert(this.firstName);
            }
            arrow();
        }
    };

    user.sayHi(); // Ilya
}

/*
 * Wat geeft de volgende code terug als resultaat?
 */
{
    let user = {
        firstName: "Ilya",
        sayHi: () => {
            let arrow = () => {
                // @ts-ignore -> omdat we anders een error krijgen nog voor we uitvoeren.
                alert(this.firstName);
            }
            arrow();
        }
    };

    user.sayHi();
}
