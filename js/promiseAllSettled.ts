/**
 * Dit voorbeeld illustreert het gebruik van Promise.allSettled om een array van Promises
 * af te handelen, waarbij sommige beloften slagen en andere falen.
 * De resultaten worden vervolgens afgedrukt met de bijbehorende statussen en waarden of redenen.
 */

// Functie die een Promise teruggeeft die na een bepaalde tijd slaagt of faalt
// @ts-ignore
function delay(ms: number, value: any, shouldReject: boolean): Promise<string> {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (shouldReject) {
                reject(value);
            } else {
                resolve(value);
            }
        }, ms);
    });
}

// Array van beloften die enkele beloften bevat die slagen en enkele die falen
// @ts-ignore
const promises: Promise<any>[] = [
    delay(1000, 'Eerste belofte', false),
    delay(2000, 'Tweede belofte', true),
    delay(3000, 'Derde belofte', false)
];

// Wachten tot alle beloften zijn afgehandeld (ongeacht of ze slagen of falen)
Promise.allSettled(promises)
    .then(results => {

        results.forEach((result, index) => {

            if (result.status === 'fulfilled') {
                console.log(`Belofte ${index + 1} is geslaagd met waarde:`, result.value);
            } else if (result.status === 'rejected') {
                console.log(`Belofte ${index + 1} is mislukt met reden:`, result.reason);
            }

        });
    });