/**
 * In dit voorbeeld worden alle beloften uitgevoerd met behulp van Promise.all.
 * De .then-methode wordt gebruikt om de resultaten af te handelen als alle promises
 * met succes zijn voltooid, en de .catch-methode wordt gebruikt om eventuele fouten
 * af te handelen die zich voordoen tijdens het uitvoeren van de beloften.
 * */

// Functie die een Promise teruggeeft die na een bepaalde tijd slaagt of faalt
// @ts-ignore
function delay(ms: number, value: any, shouldReject: boolean): Promise<string> {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (shouldReject) {
                reject(value);
            } else {
                resolve(value);
            }
        }, ms);
    });
}

// Array van beloften die enkele beloften bevat die slagen en enkele die falen
// @ts-ignore
const promises: Promise<any>[] = [
    delay(1000, 'Eerste belofte', false),
    delay(2000, 'Tweede belofte', true),
    delay(3000, 'Derde belofte', false)
];

// Wachten tot alle beloften zijn afgehandeld (alleen als ze allemaal slagen)
Promise.all(promises)
    .then(results => {
        console.log('Alle beloften zijn geslaagd met waarden:', results);
    })
    .catch(error => {
        console.log('Een van de beloften is mislukt met fout:', error);
    });
