// avoid tsc warning because this function is also in another file
export {};

async function produce() {
  return "AI found result";
}

async function consume() {
  console.log(await produce());
}

consume();
console.log("Thinking first…");